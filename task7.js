// Task 1

let friends_0 = ["George", "Nick", "Tom", "Kate", "Annie"];
let friends_1 = ["James", "Will", "Jack", "Nate", "Edward"];

function arrayFriends(array) {
  return array.filter((name) => name.length == 4);
}

console.log(arrayFriends(friends_0));
console.log(arrayFriends(friends_1));

// Task 2

let numbers_0 = [5, 8, 12, 19, 22];
let numbers_1 = [52, 76, 14, 12, 4];
let numbers_2 = [3, 87, 45, 12, 7];

function arraySum(array) {
  array.sort((a, b) => a - b);
   return (array.length >= 4) ? array[0] + array[1] : "false"
  }

console.log(`First Sum Is ${arraySum(numbers_0)}`);
console.log(`Second Sum Is ${arraySum(numbers_1)}`);
console.log(`Third Sum Is ${arraySum(numbers_2)}`);